import { View, Image } from 'react-native'
import React from 'react'
import styles from '../assets/styles';

const Header = () => {
  const image_logo = require('../assets/logo.png');

  return (
    <View style={styles.header}>
    <Image 
      style={{ 
        width: '80%',
        height: 65,
      }} 
      source={image_logo} />
  </View>
  );
}

export default Header

