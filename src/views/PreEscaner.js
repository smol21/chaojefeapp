import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import Header from '../components/Header';
import SocialNetworks from '../components/SocialNetworks';
import Escaner from './Escaner';

const PreEscaner = () => {
  return (
    <View style={{flex:1}}>
      <Header />
      <View style={{flex:0}}>
        <ScrollView>
          <Escaner />
        </ScrollView>
      </View>
      <SocialNetworks />
    </View>
  );
};

export default PreEscaner;
