import {Dimensions, Platform} from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const DeviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = {
  scrollViewStyle: {
    flex: 1,
    backgroundColor: '#00000000',
  },
  btn_general: {
    backgroundColor: '#ed1c24',
    marginBottom: Platform.OS === 'android' ? 15 : 0,

    width: deviceWidth - 200,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderWidth: 3,
    borderRadius: 50,
    borderColor: '#fff',
  },
  btn_text: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Fredoka-Medium',
  },
  // Headers
  header: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#a82b93',
  },
    tinyLogo: {
    width: '80%',
    height: 90,
  },
  tinyLogoRedes: {
    width: 30,
    height: 30,
  },
  //footer
  footer: {
    backgroundColor: '#a82b93',
    alignItems: 'center',
    height: 50,
    flex: 0.8,
  },
  borderFooter: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },
   //CUADRICULA
   estilosPrincipal: {
    flex: 3,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  textTitle1: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    padding: 15,
    color: 'white'
},
textTitle2: {
  fontWeight: 'bold',
  fontSize: 23,
  marginTop: 15,
  textAlign: 'center',
  color: '#fff'
},
marginText: {
  marginTop: 25,
  textAlign: 'center',
},
  borderCuariculas: {
    flexDirection: 'row',
    marginTop: Platform.OS === 'android' ? 0 : 30,
  },

  estiloNumeros: {
    width: DeviceWidth * 0.15,
    height: DeviceWidth * 0.1,
  },
  estiloNumeros_: {
    width: DeviceWidth * 0.13,
    height: DeviceWidth * 0.13,
  },


  montosPremios: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  esitloMontos: {
    fontSize: 20,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos2: {
    fontSize: 25,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos3: {
    fontSize: 15,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos4: {
    fontSize: 10,
    fontWeight: '900',
    color: '#0a0d64',
  },
  esitloMontos5: {
    fontSize: 12,
    fontWeight: '900',
    color: '#0a0d64',
  },
  tinyNumeros: {
    width: 25,
    height: 25,
  },
  tinyFlechas: {
    width: 15,
    height: 25,
  },
    tinyNumeros2: {
    width: 35,
    height: 25,
  },
  tinyFlechas2: {
    width: 40,
    height: 25,
  },
  celda:{
    width: DeviceWidth * 0.26,
    height: DeviceWidth * 0.17,
    backgroundColor: 'transparent',
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  textPoliticas:{
    fontSize: 15,
    fontWeight: '900',
    color: '#fff',
    textDecorationLine: 'underline'
  },
};

export default styles;
