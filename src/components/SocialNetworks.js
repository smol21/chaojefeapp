import {View, Image, Dimensions, TouchableOpacity, Linking, Text} from 'react-native';
import styles from '../assets/styles';
import React from 'react';

const SocialNetworks = () => {
    const DeviceWidth = Dimensions.get('window').width;
  return (
    <View style={styles.footer}>
      <View style={styles.borderFooter}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://twitter.com/chaojefeoficial')
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../assets/twitter.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://www.facebook.com/people/chao-jefe/61552048103873/')
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../assets/facebook.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.15,
                marginBottom: 1,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://www.instagram.com/chaojefeoficial/')
                }>
                <Image
                  style={styles.tinyLogoRedes}
                  source={require('../assets/instagram.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            marginTop: 35,
            flex: 2,
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() =>
              Linking.openURL(
                'https://chaojefe-oficial.com/politicas-de-seguridad/',
              )
            }>
            <Text style={styles.textPoliticas}>
              Políticas de Seguridad
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default SocialNetworks