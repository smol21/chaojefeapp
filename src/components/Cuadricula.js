import {View, Text, Dimensions, Image, Alert,StyleSheet} from 'react-native';
import React from 'react';
import styles from '../assets/styles';
import scanStyle from '../assets/scanStyle';
import Celda from '../components/Celda';

const Cuadricula = (data) => {
  const DeviceWidth = Dimensions.get('window').width;
  const image_cuadricula = require('../assets/cuadricula.png');

  const reg = new RegExp('^[1-8]*$');

  let resultValor = data.valor;
  const datosCuadricula = [];
  datosCuadricula.push(resultValor.substring(3, 12));
  const arrayCuadricula = [...datosCuadricula[0]];

  if (reg.test(resultValor.substring(3, 12))) {
    console.log("si ENTRA AQUI")
    /*AQUI AGRUPAMOS LOS CARACTERES PARA SABER CUANTOS HAY POR CADA UNO*/
    let agrupados = {}
    arrayCuadricula.forEach( x => {
      if( !agrupados.hasOwnProperty(x)){
        agrupados[x] = 0
      }
      agrupados[x] += 1;
    })

    const data = agrupados;
    const mensajes = (props) => {
      Alert.alert('¡Felicidades!', 'Eres ganador de' + ' ' + props);
    };

      if(data.hasOwnProperty("1") && data["1"] == 3){
        mensajes('$ 1');
      }else if(data.hasOwnProperty("2") && data["2"] == 3){
        mensajes('$ 2');
      }else if(data.hasOwnProperty("3") && data["3"] == 3){
        mensajes('$ 3');
      } else if(data.hasOwnProperty("4") && data["4"] == 3){
        mensajes('$ 10');
      } else if(data.hasOwnProperty("5") && data["5"] == 3){
        mensajes('$ 20');
      } else if(data.hasOwnProperty("6") && data["6"] == 3){
        mensajes('$ 50');
      }else if(data.hasOwnProperty("7") && data["7"] == 3){
        mensajes(' un Teléfono.');
      }else if(data.hasOwnProperty("8") && data["8"] == 3){
        mensajes('$ 12.000');
      }
      else{
        Alert.alert('¡Lo sentimos!','No eres ganador');
          
      }
  }   

  return (
    <View style={{flex:1}}>
      <View style={{...styles.marginText,flex:1}}>
              <Text style={[styles22.paragraph]}>
                Gana al Instante
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: -2}},
                ]}>
                Gana al Instante
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: 2}},
                ]}>
                Gana al Instante
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: 2, height: -2}},
                ]}>
                Gana al Instante
              </Text>
               <Text style={scanStyle.descText2}>
                  Sueldo durante un año, Teléfonos y MUCHO dinero.
                </Text>

      </View>
      <View style={styles.estilosPrincipal}>
        <Image
          source={image_cuadricula}
          resizeMode="center"
          style={{
            position: 'absolute',
            width: '80%',
            height: '80%',
          }}
        />
        <View style={styles.borderCuariculas}>
          {/* INICIO COLUMNA 0 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                marginTop: 20,
              }}>
            </View>
          </View>
          {/* INICIO COLUMNA 1 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[0]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[3]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                borderBottomLeftRadius: 20,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[6]} />
            </View>

            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
          </View>
          {/* INICIO COLUMNA 2 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
             <Celda value={arrayCuadricula[1]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[4]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[7]} />
            </View>

            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
          </View>
          {/* INICIO COLUMNA 3 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[2]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[5]} />
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
                borderBottomRightRadius: 20,
                flex: 1,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Celda value={arrayCuadricula[8]} />
            </View>

            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
          </View>
          {/* INICIO COLUMNA 4 */}
          <View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>

            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
            <View
              style={{
                width: DeviceWidth * 0.19,
                height: DeviceWidth * 0.19,
                backgroundColor: 'transparent',
              }}>
              <></>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles22 = StyleSheet.create({
  paragraph: {
    fontSize: 35,
    fontFamily:'Fredoka-Medium',
    fontWeight:700,
    textAlign: 'center', // <-- the magic
    color: '#00a651',
    textShadowColor: '#fff',
    textShadowRadius: 1,
    textShadowOffset: {
      width: 2,
      height: 2,
    },
  },
  abs: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default Cuadricula;
