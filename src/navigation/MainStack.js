import React from 'react';
import InicioChaoJefe from '../views/InicioChaoJefe';
import PreEscaner from '../views/PreEscaner';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
			<Stack.Screen name="InicioChaoJefe" component={InicioChaoJefe} />
			<Stack.Screen name="PreEscaner" component={PreEscaner} />
		</Stack.Navigator>
  );
};

export default MainStack;
