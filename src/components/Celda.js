import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import styles from '../assets/styles';
import _interopRequireDefault from '@babel/runtime/helpers/interopRequireDefault';

const Celda = (props) => {
  return (
    <View>
      {props.value === "1" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_8.png')} />
      )}
      {props.value === "2" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_3.png')} />
      )}
      {props.value === "3" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_6.png')} />
      )}
      {props.value === "4" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_2.png')} />
      )}
      {props.value === "5" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_4.png')} />
      )}
      {props.value === "6" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_5.png')} />
      )}
      {props.value === "7" && (
          <Image style={styles.estiloNumeros} source={require('../assets/Premio_7.png')}/>
      )}
      {props.value === "8" && (
          <Image style={styles.estiloNumeros_} source={require('../assets/Premio_1.png')}/>
      )}
    </View>
  )
}

export default Celda
