import React from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from '../assets/styles';

const InicioChaoJefe = () => {
  const image_fondo = require('../assets/Pantalla_Inicio.jpg');
  const navigation = useNavigation();

  return (
    <>
      <View style={{flex: 1}}>
        <Image
          source={image_fondo}
          resizeMode="stretch"
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
        />
        <ScrollView
          style={styles.scrollViewStyle}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <View>
            <TouchableOpacity
              style={styles.btn_general}
              onPress={() => navigation.navigate('PreEscaner')}>
              <Text style={styles.btn_text}>Validar Ticket</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default InicioChaoJefe;
