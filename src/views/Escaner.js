import React, {Fragment, useState} from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Image,
  ImageBackground,
  Alert,
} from 'react-native';

import QRCodeScanner from 'react-native-infy-qrcode-scanner';
import {RNCamera} from 'react-native-infy-camera';
import scanStyle from '../assets/scanStyle';
import Cuadricula from '../components/Cuadricula';

const Escaner = () => {
  const image = require('../assets/fondo.jpg');
  const [state, setState] = useState({
    scan: false,
    ScanResult: false,
    result: null,
    datosSorteo: '',
  });

  onSuccess = e => {
    console.log("entro en on Scuccer")
    state.datosSorteo = e.data;
    if (state.datosSorteo) {
      console.log("existe data",state.datosSorteo)
      setState({
        ...state,
        result: e,
        scan: false,
        ScanResult: true,
        datosSorteo: e.data,
      });
    } else {
      Alert.alert(
        '¡No se pudo leer el código QR!',
        'Por favor intente nuevamente',
      );
    }
  };

  const activeQR = () => {
    setState({
      ...state,
      scan: true,
    });
  };
  const scanAgain = () => {
    setState({
      ...state,
      scan: true,
      ScanResult: false,
    });
  };

  return (
    <View style={{flex: 1}}>
      <Image
        source={image}
        resizeMode="cover"
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
        }}
      />
      <Fragment>
        {!state.scan && !state.ScanResult && (
          <View style={scanStyle.cardView}>
            <View>
              <Text style={[styles22.paragraph]}>
                Mueva su cámara
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: -2}},
                ]}>
                Mueva su cámara
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: 2}},
                ]}>
                Mueva su cámara
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: 2, height: -2}},
                ]}>
                Mueva su cámara
              </Text>
               <Text style={scanStyle.descText2}>
                  sobre el código QR
                </Text>
            </View>
            <Image
              source={require('../assets/qr.png')}
              style={{margin: 10, width: 250, height: 250}}></Image>
            <TouchableOpacity
              onPress={() => activeQR()}
              style={scanStyle.buttonScan}>
              <View style={scanStyle.buttonWrapper}>
                <Image
                  source={require('../assets/camara_nueva.png')}
                  style={{height: 35, width: 35}}></Image>
                <Text
                  style={{
                    ...scanStyle.buttonTextStyle,
                    color: '#fff',
                    marginLeft: 10,
                  }}>
                  Escanear código QR
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {state.ScanResult && (
          <View>
            {state.datosSorteo.length === 21 ? (
              <View style={scanStyle.cardViewCuadricula}>
                <Cuadricula valor={state.datosSorteo} />
                <TouchableOpacity
                  onPress={() => scanAgain()}
                  style={{...scanStyle.buttonScan3}}>
                  <View style={scanStyle.buttonWrapper}>
                    <Text
                      style={scanStyle.buttonTextStyle2}>
                      VALIDAR DE NUEVO
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{...scanStyle.cardView}}>
                <View
                  style={{
                    alignItems: 'center',
                  }}>
                    <View>
              <Text style={[styles22.paragraph]}>
                ¡No se pudo leer el código QR!
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: -2}},
                ]}>
                ¡No se pudo leer el código QR!
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: -2, height: 2}},
                ]}>
                ¡No se pudo leer el código QR!
              </Text>
              <Text
                style={[
                  styles22.paragraph,
                  styles22.abs,
                  {textShadowOffset: {width: 2, height: -2}},
                ]}>
                ¡No se pudo leer el código QR!
              </Text>
              <Text style={scanStyle.descText2}>
                    Por favor intente nuevamente
                  </Text>
            </View>
                  
                </View>
                <Image
                  source={require('../assets/qr.png')}
                  style={{margin: 10, width: 250, height: 250}}></Image>
                <TouchableOpacity
                  onPress={() => scanAgain()}
                  style={{...scanStyle.buttonScan}}>
                  <View style={scanStyle.buttonWrapper}>
                    <Image
                      source={require('../assets/camara_nueva.png')}
                      style={{height: 35, width: 35}}></Image>
                    <Text
                      style={{...scanStyle.buttonTextStyle2, marginLeft: 10}}>
                      Escanear de nuevo
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
        {state.scan && (
          <QRCodeScanner
            onRead={e => onSuccess(e)}
            // flashMode={RNCamera.Constants.FlashMode.torch}
            topContent={
              <View>
                <ImageBackground
                  // source={require('../assets/top-panel.png')}
                  style={scanStyle.topContent}></ImageBackground>
              </View>
            }
            bottomContent={
              <View>
                <ImageBackground
                  // source={require('../assets/bottom-panel.png')}
                  style={scanStyle.bottomContent}>
                  <TouchableOpacity
                    style={scanStyle.buttonScan2}
                    onPress={() => setState({scan: false})}>
                    <Image
                      source={require('../assets/camara_morada.png')}
                      style={{width: 100, height: 100}}></Image>
                  </TouchableOpacity>
                </ImageBackground>
              </View>
            }
          />
        )}
      </Fragment>
    </View>
  );
};
const styles22 = StyleSheet.create({
  paragraph: {
    fontSize: 35,
    fontFamily: 'Fredoka-Medium',
    fontWeight:700,
    textAlign: 'center', // <-- the magic
    color: '#00a651',
    textShadowColor: '#fff',
    textShadowRadius: 1,
    textShadowOffset: {
      width: 2,
      height: 2,
    },
  },
  abs: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
export default Escaner;
